<?php
// 1. Koppla upp sig mot databasen.
// server, login, password, database.
$link = mysqli_connect("localhost", "lektion", "lektion", "lektion");
// Säg till databasen att man vill ha ut resultatet i utf8.
mysqli_set_charset($link, "utf8");

if (!empty($_POST['headline'])) {
  $query = "INSERT INTO `items` (`item_id`, `headline`, `text`, `date`) VALUES (NULL, '" . $_POST['headline'] . "', '" . $_POST['text'] . "', NOW())";
  mysqli_query($link, $query);
}
print_r($_POST);

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
          crossorigin="anonymous">
    <title>PHP och MySQL</title>
</head>
<body>
<form action="" method="post">
    <input type="text" name="headline"><br>
    <textarea name="text"></textarea>
    <br>
    <input type="submit" name="submit" value="Spara">
</form>
<table class="table table-hover table-striped">
    <thead>
    <tr>
        <th scope="col">Headline</th>
        <th scope="col">Text</th>
        <th scope="col">date</th>
    </tr>
    </thead>
    <tbody>
    <?php

    // 1 behöver man bara göra en gång, detta kan man med fördel ha i en fil man tar include på.
    // 2 och 3 gör man ofta flera gånger per sida.


    // Det som finns ovan brukar jag ha i en egen fil, använder mig av include.

    // 2. Ställ en fråga till databasen.
    // Skapa frågan som sträng.
    echo $query = "SELECT * FROM items ORDER BY item_id DESC";

    // Ställ frågan.
    $result = mysqli_query($link, $query);
    // Skriv ut eventuella fel.
    echo mysqli_error($link);

    // 3. Skriv ut svaret med hjälp av en while-loop.
    $n = 0;
    while ($row = mysqli_fetch_assoc($result)) {
      $n++;
      echo '
          <tr>
            <td>' . $row['headline'] . '</td>
            <td>' . $row['text'] . '</td>
            <td>' . $row['date'] . '</td>
          </tr>';
    }
    echo '</tbody></table>';

    ?>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>
</html>
