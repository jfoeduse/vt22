<?php

/**
 * @file
 * How token-based authentication works.
 *
 * In token-based authentication, the client exchanges hard
 * credentials (such as username and password) for a piece of data called
 * token. For each request, instead of sending the hard credentials, the client
 * will send the token to the server to perform authentication and then
 * authorization.
 *
 * In a few words, an authentication scheme based on tokens follow these steps:
 *
 * 1. The client sends their credentials (username and password) to the server.
 * 2. The server authenticates the credentials and, if they are valid, generate
 *    a token for the user.
 * 3. The server stores the previously generated token in some storage along
 *    with the user identifier and an expiration date.
 * 4. The server sends the encrypted generated token to the client.
 * 5. The client sends the token to the server in each request.
 * 6. The server, in each request, extracts the token from the incoming
 *    request. With the token, the server looks up the user details to perform
 *    authentication.
 * 7. If the token is valid, the server accepts the request. If the token is
 *    invalid, the server refuses the request.
 * 8. Once the authentication has been performed, the server performs the
 *    request.
 * 9. The server can provide an endpoint to refresh tokens.
 *
 * This endpoint provides a refreshed token if credentials are correct.
 *
 * https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 */

if (empty($_POST['login']) || empty($_POST['password'])) {
  $data = [
    'status_code' => 401,
    'status_message' => 'Missing some credentials',
    'post_data' => $_POST,
  ];
  api_response($data);
}

// Authenticates the credentials.
$query = "SELECT * FROM `glusers` WHERE `login` = '" . $_POST['login'] . "' AND password = SHA1('" . SALT . $_POST['password'] . "')";
$result = query($query);
if (mysqli_num_rows($result) != 1) {
  $data = [
    'status_code' => 401,
    'status_message' => 'Incorrect credentials',
    'post_data' => $_POST,
  ];
  api_response($data);
}
$row = mysqli_fetch_assoc($result);

// Generate new token.
$token = sha1($row['login'] . SALT . microtime() . $row['password']);
$query = "UPDATE `glusers` SET `token` = '" . $token . "', `token_datetime` = NOW() WHERE `id` = '" . $row['id'] . "'";
query($query);

$data = [
  'status_code' => 200,
  'auth_token' => $token,
  'status_message' => 'Success',
];
api_response($data);
