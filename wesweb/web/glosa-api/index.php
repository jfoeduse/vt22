<?php

/**
 * @file
 * Router "ish" file.
 */

include 'include/setup.php';
// Using rewrite rules to get clean URL:s.
if (isset($_SERVER['REDIRECT_QUERY_STRING'])) {
  $request_array = explode('/', $_SERVER['REDIRECT_QUERY_STRING']);
  if (is_array($request_array)) {
    $request_array = array_map('my_filter', $request_array, array_pad([], count($request_array), 'endpoint'));
  }
}
//print_r($request_array);
// Check if we have an endpoint.
if (empty($request_array[0])) {
  // No Endpoint, exit.
  api_response();
}
$endpoint_name = $request_array[0];

// Cleanup $_POST.
if (is_array($_POST)) {
  $_POST = array_map('my_filter', $_POST);
}

// Get the endpoint.
$endpoint = __DIR__ . '/endpoints/' . $endpoint_name . '.php';
$endpoints = glob(__DIR__ . '/endpoints/*.php');
$_user_id = FALSE;
if (in_array($endpoint, $endpoints)) {
  $_user_id = authorize();
}
else {
  // Get the public endpoint...
  $endpoint = __DIR__ . '/public_endpoints/' . $endpoint_name . '.php';
  $endpoints = glob(__DIR__ . '/public_endpoints/*.php');
}

// Include endpoint if exists.
if (file_exists($endpoint) && in_array($endpoint, $endpoints)) {
  include $endpoint;
}
// Endpoint do not exist.
api_response();
