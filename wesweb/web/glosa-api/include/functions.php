<?php

/**
 * @file
 * Put your common functions in this file...
 */

/**
 * Send all db-query to this function.
 *
 * @param string $query
 *   Query-string.
 *
 * @return bool|\mysqli_result
 *   Return mysqli_result or FALSE if error.
 */
function query($query) {
  global $_link;
  if ($result = mysqli_query($_link, $query)) {
    return $result;
  }
  elseif (DEBUG_STATUS) {
    ob_start();
    debug_print_backtrace();
    $trace = ob_get_contents();
    ob_end_clean();
    $data = [
      'status_code' => 400,
      'backtrace' => $trace,
      'error' => mysqli_error($_link),
      'query' => $query,
    ];
    api_response($data);
  }
  else {
    $data = [
      'status_code' => 400,
    ];
    api_response($data);
  }
}

/**
 * Filter a value.
 *
 * @param string $value
 *   Value to filer.
 * @param string $type
 *   Data source.
 *
 * @return string
 *   Filtered string.
 */
function my_filter($value, $type = 'post') {
  global $_link;
  $value = trim($value);
  if ($type == 'endpoint') {
    $search = ['.', '-'];
    $replace = ['', '_'];
    $value = str_replace($search, $replace, $value);
  }
  if ($type == 'post') {
    $value = htmlspecialchars($value);
  }
  return mysqli_real_escape_string($_link, $value);
}

/**
 * Validate token.
 *
 *  Exit if token is invalid.
 *  Return ID of User if OK.
 */
function authorize() {
  if (!isset($_POST['auth_token'])) {
    $data = [
      'status_code' => 400,
    ];
    api_response($data);
  }
  $query = "SELECT * FROM `glusers` WHERE `token` = '" . $_POST['auth_token'] . "' AND DATE_ADD(`token_datetime`, INTERVAL " . TOKEN_LIFE . " DAY) > NOW()";
  $result = query($query);
  if (mysqli_num_rows($result) != 1) {
    $data = [
      'status_message' => __FUNCTION__,
      'status_code' => 401,
    ];
    api_response($data);
  }
  $row = mysqli_fetch_assoc($result);
  return $row['id'];
}

/**
 * Send json-data and exit.
 *
 * @param array $data
 *   PHP-array.
 */
function api_response(array $data = []) {
  if (empty($data['status_code'])) {
    $data = [
      'status_code' => 404,
      'status_message' => 'Not Found',
    ];
  }
  header_status($data['status_code']);
  header('Content-Type: application/json');
  echo json_encode($data);
  exit;
}

/**
 * Send header with status code.
 *
 * @param int $status_code
 *   Statuscode.
 */
function header_status($status_code) {
  static $status_codes = NULL;

  if ($status_codes === NULL) {
    $status_codes = [
      100 => 'Continue',
      101 => 'Switching Protocols',
      102 => 'Processing',
      200 => 'OK',
      201 => 'Created',
      202 => 'Accepted',
      203 => 'Non-Authoritative Information',
      204 => 'No Content',
      205 => 'Reset Content',
      206 => 'Partial Content',
      207 => 'Multi-Status',
      300 => 'Multiple Choices',
      301 => 'Moved Permanently',
      302 => 'Found',
      303 => 'See Other',
      304 => 'Not Modified',
      305 => 'Use Proxy',
      307 => 'Temporary Redirect',
      400 => 'Bad Request',
      401 => 'Unauthorized',
      402 => 'Payment Required',
      403 => 'Forbidden',
      404 => 'Not Found',
      405 => 'Method Not Allowed',
      406 => 'Not Acceptable',
      407 => 'Proxy Authentication Required',
      408 => 'Request Timeout',
      409 => 'Conflict',
      410 => 'Gone',
      411 => 'Length Required',
      412 => 'Precondition Failed',
      413 => 'Request Entity Too Large',
      414 => 'Request-URI Too Long',
      415 => 'Unsupported Media Type',
      416 => 'Requested Range Not Satisfiable',
      417 => 'Expectation Failed',
      418 => 'Im a teapot',
      422 => 'Unprocessable Entity',
      423 => 'Locked',
      424 => 'Failed Dependency',
      425 => 'Too Early',
      426 => 'Upgrade Required',
      500 => 'Internal Server Error',
      501 => 'Not Implemented',
      502 => 'Bad Gateway',
      503 => 'Service Unavailable',
      504 => 'Gateway Timeout',
      505 => 'HTTP Version Not Supported',
      506 => 'Variant Also Negotiates',
      507 => 'Insufficient Storage',
      509 => 'Bandwidth Limit Exceeded',
      510 => 'Not Extended',
    ];
  }

  if (!empty($status_codes[$status_code])) {
    $status_string = $status_code . ' ' . $status_codes[$status_code];
    header($_SERVER['SERVER_PROTOCOL'] . ' ' . $status_string, TRUE, $status_code);
//    Korrekt rad ovan är lite jobbigare att hantera i ert Java - program, vi "fuskar" och skickar alltid en 200.
//     header($_SERVER['SERVER_PROTOCOL'] . ' ' . $status_string, TRUE, 200);
  }
}
