# Glosa API ska finnas på:
~login/wesweb/web/glosa-api

## Tips och tänk på.
1. Kopiera settings.php.default till settings.php, se till att in inte skicka upp er settings.php till Gitlab.
2. Flytta er till katalogen glosa-api/db, kör ``php updatedb.php``  
3. Vill ni underlätta er Java kodning kan ni "fuska" genom att alltid skicka en 200, se längst ner i funktionen header_status() 

## Krav på koden:
* All kod ni skriver ska följa samma struktur, om ni är osäker på vad det innebär fråga.
* Varje endpoint ska svara med minst detta: 
  * status_code
  * status_message
* All data ska skickas vi $_POST
* Endpoint bestäms via url ([a-z] och '-')  
* I Talend API Tester (eller liknade verktyg) ska ni spara minst ett "request" för varje endpoint.
