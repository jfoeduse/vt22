<?php

/**
 * @file
 * Version 1.
 */

$sql[] = "
CREATE TABLE `db_version` (
  `id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_swedish_ci;
";

$sql[] = "
CREATE TABLE `glusers` (
`id` int(11) NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_swedish_ci NOT NULL,
  `permissions` int(11) NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8mb4_swedish_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_swedish_ci NOT NULL,
  `token_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_swedish_ci;
";

$sql[] = "
ALTER TABLE `db_version`
  ADD PRIMARY KEY (`id`);
";

$sql[] = "
ALTER TABLE `glusers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `token_datetime` (`token_datetime`);
";

$sql[] = "
ALTER TABLE `db_version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
";

$sql[] = "
ALTER TABLE `glusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
";
