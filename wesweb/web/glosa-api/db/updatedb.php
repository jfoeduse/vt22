<?php

/**
 * @file
 * Script for DB updates.
 *
 *  One file with name codeversion.php must exist
 *  and one file for each DB-version.
 *
 * From cli run script with 'php updatedb.php'
 */

include __DIR__ . '/../include/setup.php';

echo 'Update db running...';

// Check Code-version.
include __DIR__ . '/codeversion.php';
echo "\nCode-version is: " . $code_version;


// Check if DB exists.
$result = query("SHOW TABLES LIKE 'db_version'");
$n = mysqli_num_rows($result);

if ($n != 0) {
  // Get last DB-version.
  $result = query("SELECT * FROM `db_version` ORDER BY `id` DESC");
  $row = mysqli_fetch_assoc($result);
  $db_version = $row['version'];
}
else {
  // No DB-version exists.
  $db_version = 0;
}

echo "\nDatabasens version är: " . $db_version . "\n";

// While DB-version is less then Code-version.
while ($db_version < $code_version) {
  $db_version++;
  if (file_exists(__DIR__ . '/' . $db_version . '.php')) {
    unset($sql);
    echo "\nUpdate DB to version: $db_version\n";
    include __DIR__ . '/' . $db_version . '.php';
    if (isset($sql) && is_array($sql)) {
      foreach ($sql as $query) {
        $result = query($query);
      }
    }
    $result = query("INSERT INTO  `db_version` (`id` ,`version` ,`date`) VALUES (NULL ,  '" . $db_version . "', NOW())");
  }
  else {
    echo "\nERROR! Not good, the file to DB-version: $db_version is missing, exit...\n";
    exit;
  }
}
