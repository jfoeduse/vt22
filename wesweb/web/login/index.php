<?php

/**
 * @file
 * Main file, router "ish".
 *
 * $_GET['p'] (page = page with html)
 *  => $is_page = TRUE.
 *
 * $_GET['c'] (controller = page with only php and a header(.)
 *  => $is_page = FALSE.
 */

$path = __DIR__ . '/app';
include_once $path . '/include/functions.php';

$is_page = TRUE;
if (isset($_GET['c'])) {
  $_GET['p'] = $_GET['c'];
  $is_page = FALSE;
}

// Fix start page.
if (!isset($_GET['p'])) {
  $get_page = 'login_form';
}
else {
  $search = ['.', '_', '-'];
  $replace = ['', '', '_'];
  $get_page = str_replace($search, $replace, $_GET['p']);
}

// Get path to the page and check if page exists.
$page = $path . '/pages/' . $get_page . '.php';
$pages = glob($path . '/pages/*.php');
if (in_array($page, $pages)) {
  authorize();
}
else {
  // Get the public pages.
  $page = $path . '/public_pages/' . $get_page . '.php';
  $pages = glob($path . '/public_pages/*.php');
}

// Include the requested files if exists...
if ($is_page) {
  include_once $path . '/include/head.php';
}
if (file_exists($page) && in_array($page, $pages)) {
  include $page;
}
if ($is_page) {
  include_once $path . '/include/foot.php';
}
