<form method="post" action="index.php?c=do-login">
  <div class="mb-3">
    <label for="InputEmail1" class="form-label">Email address</label>
    <input type="email" name="email" class="form-control" id="InputEmail1" aria-describedby="emailHelp">
    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>
  <div class="mb-3">
    <label for="InputPassword1" class="form-label">Password</label>
    <input type="password" name="password" class="form-control" id="InputPassword1">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
