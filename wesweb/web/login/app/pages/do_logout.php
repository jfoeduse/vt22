<?php
unset($_SESSION['user']);
set_message('Logged out successfully', 'success');
header('location: index.php');
exit;
