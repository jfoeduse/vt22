<?php

session_name('ab1337');
session_start();
define('DEBUG_STATUS', TRUE);


include_once __DIR__ . '/settings.php';
$_link = mysqli_connect($hostname, $user, $password, $db);


/**
 * Check if authorized.
 */
function authorize() {
  if (empty($_SESSION['user']['user_id'])) {
    header('location: index.php');
    exit;
  }
}

/**
 * Send all db-query to this function.
 *
 * @param string $query
 *   Query-string.
 *
 * @return bool|\mysqli_result
 *   Return mysqli_result or FALSE if error.
 */
function query($query) {
  global $_link;
  if ($result = mysqli_query($_link, $query)) {
    return $result;
  }
  elseif (DEBUG_STATUS) {
    ob_start();
    debug_print_backtrace();
    $trace = ob_get_contents();
    ob_end_clean();
    $trace = explode('#', $trace);
    $data = [
      'status_code' => 400,
      'backtrace' => $trace,
      'error' => mysqli_error($_link),
      'query' => $query,
    ];
    echo '<pre>';
    print_r($data);
    exit;
  }
  else {
    // Some logging??...
  }
  return FALSE;
}

/**
 * Set message function.
 *
 * @param string $message
 *   The message.
 * @param string $type
 *   Bootstrap typ of message.
 */
function set_message($message, $type = 'info') {
  $types = ['success', 'info', 'warning', 'danger'];
  if (in_array($type, $types)) {
    $_SESSION['message'][$type][] = $message;
  }
  else {
    set_message($type . ' is unknown type', 'danger');
  }
}


function get_messages() {
  if (!empty($_SESSION['message'])) {
    foreach ($_SESSION['message'] as $key1 => $type) {
      if (!empty($type)) {
        foreach ($type as $message) {
          echo '<div class="alert alert-' . $key1 . ' alert-dismissible fade show" role="alert">
                   <strong>' . strtoupper($key1) . '!</strong> ' . $message . '
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>';
        }
      }
    }
  }
  unset($_SESSION['message']);
}
